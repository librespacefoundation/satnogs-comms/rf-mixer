EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title "RF-Mixer based on RFFC2071"
Date "2020-03-09"
Rev ""
Comp ""
Comment1 "CERN Open Hardware Licence v1.2 "
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L lsf-kicad:RFFC2071 U1
U 1 1 5E2A5B7B
P 2650 3475
F 0 "U1" H 2650 3525 50  0000 C CNN
F 1 "RFFC2071" H 2650 3400 50  0000 C CNN
F 2 "Package_DFN_QFN:QFN-32-1EP_5x5mm_P0.5mm_EP3.7x3.7mm_ThermalVias" H 2650 3475 50  0001 C CNN
F 3 "https://www.qorvo.com/products/p/RFFC2071" H 2650 3475 50  0001 C CNN
F 4 "RFFC2071" H 2650 3475 50  0001 C CNN "Mnf."
	1    2650 3475
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5E2A7C0D
P 1250 3875
F 0 "R1" V 1457 3875 50  0000 C CNN
F 1 "51k" V 1366 3875 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 1180 3875 50  0001 C CNN
F 3 "~" H 1250 3875 50  0001 C CNN
F 4 "ERJ-2RKF5102X" H 1250 3875 50  0001 C CNN "Mnf."
	1    1250 3875
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1100 3875 925  3875
Wire Wire Line
	925  3875 925  3975
$Comp
L Device:C C8
U 1 1 5E2A9AAA
P 1650 1350
F 0 "C8" H 1765 1396 50  0000 L CNN
F 1 "10n" H 1765 1305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 1688 1200 50  0001 C CNN
F 3 "~" H 1650 1350 50  0001 C CNN
F 4 "C0402C103K9RACTU " H 1650 1350 50  0001 C CNN "Mnf."
	1    1650 1350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C12
U 1 1 5E2AAEEE
P 2050 1350
F 0 "C12" H 2165 1396 50  0000 L CNN
F 1 "33p" H 2165 1305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2088 1200 50  0001 C CNN
F 3 "~" H 2050 1350 50  0001 C CNN
F 4 "C0402C330J4RACAUTO " H 2050 1350 50  0001 C CNN "Mnf."
	1    2050 1350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C15
U 1 1 5E2AB2B1
P 2450 1350
F 0 "C15" H 2565 1396 50  0000 L CNN
F 1 "10n" H 2565 1305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2488 1200 50  0001 C CNN
F 3 "~" H 2450 1350 50  0001 C CNN
F 4 "C0402C103K9RACTU " H 2450 1350 50  0001 C CNN "Mnf."
	1    2450 1350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C18
U 1 1 5E2AB6BF
P 2850 1350
F 0 "C18" H 2965 1396 50  0000 L CNN
F 1 "33p" H 2965 1305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2888 1200 50  0001 C CNN
F 3 "~" H 2850 1350 50  0001 C CNN
F 4 "C0402C330J4RACAUTO " H 2850 1350 50  0001 C CNN "Mnf."
	1    2850 1350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C21
U 1 1 5E2ABBFE
P 3250 1350
F 0 "C21" H 3365 1396 50  0000 L CNN
F 1 "10n" H 3365 1305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3288 1200 50  0001 C CNN
F 3 "~" H 3250 1350 50  0001 C CNN
F 4 "C0402C103K9RACTU " H 3250 1350 50  0001 C CNN "Mnf."
	1    3250 1350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C25
U 1 1 5E2AC02B
P 3650 1350
F 0 "C25" H 3765 1396 50  0000 L CNN
F 1 "33p" H 3765 1305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3688 1200 50  0001 C CNN
F 3 "~" H 3650 1350 50  0001 C CNN
F 4 "C0402C330J4RACAUTO " H 3650 1350 50  0001 C CNN "Mnf."
	1    3650 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 4475 2550 4700
Wire Wire Line
	2550 4700 2650 4700
Wire Wire Line
	2750 4700 2750 4475
Wire Wire Line
	2650 4750 2650 4700
Connection ~ 2650 4700
Wire Wire Line
	2650 4700 2750 4700
Wire Wire Line
	1650 1200 1650 1050
Wire Wire Line
	3650 1050 3650 1200
Wire Wire Line
	3650 1500 3650 1625
Wire Wire Line
	3650 1625 3250 1625
Wire Wire Line
	1650 1500 1650 1625
Wire Wire Line
	2050 1200 2050 1050
Wire Wire Line
	2050 1050 2450 1050
Wire Wire Line
	2450 1200 2450 1050
Connection ~ 2450 1050
Wire Wire Line
	2850 1200 2850 1050
Wire Wire Line
	2850 1050 3250 1050
Wire Wire Line
	3250 1200 3250 1050
Wire Wire Line
	3250 1050 3650 1050
Wire Wire Line
	2050 1500 2050 1625
Wire Wire Line
	2050 1625 1650 1625
Wire Wire Line
	2450 1500 2450 1625
Connection ~ 2450 1625
Wire Wire Line
	2450 1625 2050 1625
Wire Wire Line
	2850 1500 2850 1625
Wire Wire Line
	3250 1500 3250 1625
Wire Wire Line
	3250 1625 2850 1625
Wire Wire Line
	2550 2475 2550 2375
Wire Wire Line
	2550 2375 2650 2375
Wire Wire Line
	2750 2375 2750 2475
Wire Wire Line
	2650 2475 2650 2375
Connection ~ 2650 2375
Wire Wire Line
	2650 2375 2750 2375
Wire Wire Line
	2650 2375 2650 2325
Wire Wire Line
	1650 1050 2050 1050
Connection ~ 2050 1625
Connection ~ 2050 1050
Wire Wire Line
	2450 1050 2650 1050
Wire Wire Line
	2450 1625 2650 1625
Connection ~ 2850 1625
Connection ~ 2850 1050
Connection ~ 3250 1050
Connection ~ 3250 1625
Wire Wire Line
	2650 975  2650 1050
Connection ~ 2650 1050
Wire Wire Line
	2650 1050 2850 1050
Wire Wire Line
	2650 1725 2650 1625
Connection ~ 2650 1625
Wire Wire Line
	2650 1625 2850 1625
$Comp
L lsf-kicad:KT1612A U2
U 1 1 5E306B3B
P 9525 4450
F 0 "U2" H 9195 4496 50  0000 R CNN
F 1 "KT1612A" H 9195 4405 50  0000 R CNN
F 2 "lsf-kicad-lib:KT1612" H 10125 4000 50  0001 C CNN
F 3 "https://global.kyocera.com/prdct/electro/product/pdf/kt1612_e.pdf" H 9525 4450 50  0001 C CNN
F 4 "KT1612A26000AAW19TBT" H 9525 4450 50  0001 C CNN "Mnf."
	1    9525 4450
	-1   0    0    -1  
$EndComp
$Comp
L Device:C C11
U 1 1 5E311F65
P 8775 4250
F 0 "C11" V 9027 4250 50  0000 C CNN
F 1 "1n" V 8936 4250 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 8813 4100 50  0001 C CNN
F 3 "~" H 8775 4250 50  0001 C CNN
F 4 "C0402C102K8RACAUTO " H 8775 4250 50  0001 C CNN "Mnf."
	1    8775 4250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8925 4250 9025 4250
Wire Wire Line
	9425 4950 9425 5050
Wire Wire Line
	9425 5050 9525 5050
Wire Wire Line
	9625 5050 9625 4950
Wire Wire Line
	9525 5100 9525 5050
Connection ~ 9525 5050
Wire Wire Line
	9525 5050 9625 5050
$Comp
L Device:C C22
U 1 1 5E318887
P 10400 4425
F 0 "C22" H 10515 4471 50  0000 L CNN
F 1 "10n" H 10515 4380 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10438 4275 50  0001 C CNN
F 3 "~" H 10400 4425 50  0001 C CNN
F 4 "C0402C103K9RACTU " H 10400 4425 50  0001 C CNN "Mnf."
	1    10400 4425
	1    0    0    -1  
$EndComp
Wire Wire Line
	10400 4275 10400 4175
Wire Wire Line
	10400 4575 10400 4675
Wire Wire Line
	9525 3950 9525 3825
$Comp
L Device:C C20
U 1 1 5E321C95
P 3150 5875
F 0 "C20" H 3265 5921 50  0000 L CNN
F 1 "33p" H 3265 5830 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3188 5725 50  0001 C CNN
F 3 "~" H 3150 5875 50  0001 C CNN
F 4 "C0402C330J4RACAUTO " H 3150 5875 50  0001 C CNN "Mnf."
	1    3150 5875
	1    0    0    -1  
$EndComp
$Comp
L Device:C C19
U 1 1 5E32491C
P 2775 5875
F 0 "C19" H 2890 5921 50  0000 L CNN
F 1 "33p" H 2890 5830 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2813 5725 50  0001 C CNN
F 3 "~" H 2775 5875 50  0001 C CNN
F 4 "C0402C330J4RACAUTO " H 2775 5875 50  0001 C CNN "Mnf."
	1    2775 5875
	1    0    0    -1  
$EndComp
$Comp
L Device:C C16
U 1 1 5E324DA7
P 2375 5875
F 0 "C16" H 2490 5921 50  0000 L CNN
F 1 "33p" H 2490 5830 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2413 5725 50  0001 C CNN
F 3 "~" H 2375 5875 50  0001 C CNN
F 4 "C0402C330J4RACAUTO " H 2375 5875 50  0001 C CNN "Mnf."
	1    2375 5875
	1    0    0    -1  
$EndComp
$Comp
L Device:C C13
U 1 1 5E325155
P 1975 5875
F 0 "C13" H 2090 5921 50  0000 L CNN
F 1 "33p" H 2090 5830 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2013 5725 50  0001 C CNN
F 3 "~" H 1975 5875 50  0001 C CNN
F 4 "C0402C330J4RACAUTO " H 1975 5875 50  0001 C CNN "Mnf."
	1    1975 5875
	1    0    0    -1  
$EndComp
$Comp
L Device:C C9
U 1 1 5E32567E
P 1600 5875
F 0 "C9" H 1715 5921 50  0000 L CNN
F 1 "33p" H 1715 5830 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 1638 5725 50  0001 C CNN
F 3 "~" H 1600 5875 50  0001 C CNN
F 4 "C0402C330J4RACAUTO " H 1600 5875 50  0001 C CNN "Mnf."
	1    1600 5875
	1    0    0    -1  
$EndComp
NoConn ~ 3450 3175
NoConn ~ 3450 3275
$Comp
L Device:C C24
U 1 1 5E329FA6
P 3525 5875
F 0 "C24" H 3640 5921 50  0000 L CNN
F 1 "33p" H 3640 5830 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3563 5725 50  0001 C CNN
F 3 "~" H 3525 5875 50  0001 C CNN
F 4 "C0402C330J4RACAUTO " H 3525 5875 50  0001 C CNN "Mnf."
	1    3525 5875
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 6025 1600 6175
Wire Wire Line
	1600 6175 1975 6175
Wire Wire Line
	3525 6175 3525 6025
Wire Wire Line
	3150 6025 3150 6175
Connection ~ 3150 6175
Wire Wire Line
	3150 6175 3525 6175
Wire Wire Line
	2775 6025 2775 6175
Connection ~ 2775 6175
Wire Wire Line
	2775 6175 3150 6175
Wire Wire Line
	2375 6025 2375 6175
Connection ~ 2375 6175
Wire Wire Line
	2375 6175 2575 6175
Wire Wire Line
	1975 6025 1975 6175
Connection ~ 1975 6175
Wire Wire Line
	1975 6175 2375 6175
Wire Wire Line
	1600 5725 1600 5550
Wire Wire Line
	1975 5725 1975 5550
Wire Wire Line
	2375 5725 2375 5550
Wire Wire Line
	2775 5725 2775 5550
Wire Wire Line
	3150 5725 3150 5550
Wire Wire Line
	3525 5725 3525 5550
Wire Wire Line
	1850 3675 1750 3675
Wire Wire Line
	1850 3575 1750 3575
Wire Wire Line
	1850 3475 1750 3475
Wire Wire Line
	1850 3375 1750 3375
Wire Wire Line
	1850 3175 1750 3175
Wire Wire Line
	1850 3075 1750 3075
Wire Wire Line
	1850 2675 1750 2675
Wire Wire Line
	3450 3575 3550 3575
Wire Wire Line
	3450 3675 3550 3675
Wire Wire Line
	3450 3775 3550 3775
Text Label 1750 3675 2    50   ~ 0
SDA
Text Label 1750 3575 2    50   ~ 0
SCLK
Text Label 1750 3475 2    50   ~ 0
ENX
Text Label 1750 3375 2    50   ~ 0
RST
Text Label 1600 5725 2    50   ~ 0
RST
Text Label 1975 5725 2    50   ~ 0
ENX
Text Label 2375 5725 2    50   ~ 0
SCLK
Text Label 2775 5725 2    50   ~ 0
SDA
Text Label 1750 3075 2    50   ~ 0
MODE
Text Label 3150 5725 2    50   ~ 0
MODE
Text Label 1750 3175 2    50   ~ 0
ENBL
Text Label 3525 5725 2    50   ~ 0
ENBL
Wire Wire Line
	3450 3475 3550 3475
Text Label 3550 3475 0    50   ~ 0
REF_IN
Wire Wire Line
	8625 4250 8475 4250
Text Label 8475 4250 2    50   ~ 0
REF_IN
$Comp
L Device:R R2
U 1 1 5E37A3D5
P 1600 2675
F 0 "R2" V 1393 2675 50  0000 C CNN
F 1 "47" V 1484 2675 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 1530 2675 50  0001 C CNN
F 3 "~" H 1600 2675 50  0001 C CNN
F 4 "ERJ-2RKF47R0X" H 1600 2675 50  0001 C CNN "Mnf."
	1    1600 2675
	0    1    1    0   
$EndComp
$Comp
L Device:LED D1
U 1 1 5E37B2CD
P 1225 2675
F 0 "D1" H 1218 2891 50  0000 C CNN
F 1 "150040VS73240" H 1218 2800 50  0000 C CNN
F 2 "LED_SMD:LED_0402_1005Metric" H 1225 2675 50  0001 C CNN
F 3 "~" H 1225 2675 50  0001 C CNN
F 4 "150040VS73240" H 1225 2675 50  0001 C CNN "Mnf."
	1    1225 2675
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 2675 1375 2675
Wire Wire Line
	1075 2675 925  2675
Wire Wire Line
	925  2675 925  2850
Wire Wire Line
	2575 6250 2575 6175
Connection ~ 2575 6175
Wire Wire Line
	2575 6175 2775 6175
NoConn ~ 1850 2975
NoConn ~ 1850 2875
Text Label 3550 3575 0    50   ~ 0
LFILT1
Text Label 3550 3675 0    50   ~ 0
LFILT2
Text Label 3550 3775 0    50   ~ 0
LFILT3
$Comp
L Device:R R5
U 1 1 5E3A10E9
P 8775 2500
F 0 "R5" H 8845 2546 50  0000 L CNN
F 1 "22k" H 8845 2455 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 8705 2500 50  0001 C CNN
F 3 "~" H 8775 2500 50  0001 C CNN
F 4 "ERJ-2RKF2202X" H 8775 2500 50  0001 C CNN "Mnf."
	1    8775 2500
	1    0    0    -1  
$EndComp
$Comp
L Device:C C10
U 1 1 5E3A160A
P 8775 2950
F 0 "C10" H 8890 2996 50  0000 L CNN
F 1 "180p" H 8890 2905 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 8813 2800 50  0001 C CNN
F 3 "~" H 8775 2950 50  0001 C CNN
F 4 "C0402C181K5RACAUTO " H 8775 2950 50  0001 C CNN "Mnf."
	1    8775 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	8775 2350 8775 2225
Wire Wire Line
	8775 2225 8400 2225
Wire Wire Line
	8775 3100 8775 3275
Wire Wire Line
	8775 3275 8400 3275
$Comp
L Device:C C14
U 1 1 5E3A9F20
P 9150 2500
F 0 "C14" H 9265 2546 50  0000 L CNN
F 1 "8.2p" H 9265 2455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 9188 2350 50  0001 C CNN
F 3 "~" H 9150 2500 50  0001 C CNN
F 4 "CBR04C829B5GAC " H 9150 2500 50  0001 C CNN "Mnf."
	1    9150 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	9150 2225 9150 2350
Wire Wire Line
	9150 2750 9150 2650
Wire Wire Line
	9150 2750 9425 2750
$Comp
L Device:R R6
U 1 1 5E3B282E
P 9575 2750
F 0 "R6" V 9782 2750 50  0000 C CNN
F 1 "470" V 9691 2750 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 9505 2750 50  0001 C CNN
F 3 "~" H 9575 2750 50  0001 C CNN
F 4 "ERJ-2RKF4700X" H 9575 2750 50  0001 C CNN "Mnf."
	1    9575 2750
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C17
U 1 1 5E3B7E28
P 9850 2950
F 0 "C17" H 9965 2996 50  0000 L CNN
F 1 "330p" H 9965 2905 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 9888 2800 50  0001 C CNN
F 3 "~" H 9850 2950 50  0001 C CNN
F 4 "C0402C331K5RACAUTO " H 9850 2950 50  0001 C CNN "Mnf."
	1    9850 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	9725 2750 9850 2750
Wire Wire Line
	10450 2800 10450 2750
Connection ~ 10450 2750
Wire Wire Line
	10450 2750 10625 2750
Wire Wire Line
	9850 2800 9850 2750
Connection ~ 9850 2750
Wire Wire Line
	9850 2750 9975 2750
Wire Wire Line
	9850 3100 9850 3275
Wire Wire Line
	10450 3100 10450 3275
Text Label 8400 2225 2    50   ~ 0
LFILT1
Text Label 8400 3275 2    50   ~ 0
LFILT2
Text Label 10625 2750 0    50   ~ 0
LFILT3
Wire Wire Line
	8775 2225 9150 2225
Connection ~ 8775 2225
Wire Wire Line
	10275 2750 10450 2750
$Comp
L Device:R R7
U 1 1 5E3B79DC
P 10125 2750
F 0 "R7" V 10332 2750 50  0000 C CNN
F 1 "470" V 10241 2750 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 10055 2750 50  0001 C CNN
F 3 "~" H 10125 2750 50  0001 C CNN
F 4 "ERJ-2RKF4700X" H 10125 2750 50  0001 C CNN "Mnf."
	1    10125 2750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1400 3875 1850 3875
$Comp
L Device:C C23
U 1 1 5E3B82E2
P 10450 2950
F 0 "C23" H 10565 2996 50  0000 L CNN
F 1 "330p" H 10565 2905 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10488 2800 50  0001 C CNN
F 3 "~" H 10450 2950 50  0001 C CNN
F 4 "C0402C331K5RACAUTO " H 10450 2950 50  0001 C CNN "Mnf."
	1    10450 2950
	1    0    0    -1  
$EndComp
Text Notes 4050 1525 0    50   ~ 0
RF-RX, \n2000-2120MHz,\n1:1, 50:50, 1720BL15B0050E \nWhile the mixer\ncurrent is increased,\nthe Rin goes near to 50Ω.\nApplication Note: \nIntegrated Synthesizer/Mixer\nMatching Circuits and Baluns
Text Notes 6150 1300 0    50   ~ 0
IF-RX,\n2400MHz,\n4:1,  200:50
Text Notes 6945 6375 0    50   ~ 0
IF-TX,\n2400MHz, \n1:1, 50:50, 1720BL15B0050E \nWhile the mixer\ncurrent is increased,\nthe Rin goes near to 50Ω.\nApplication Note: \nIntegrated Synthesizer/Mixer\nMatching Circuits and Baluns
Text Notes 5405 6095 0    50   ~ 0
RF-TX, \n2200-2300MHz,\n4:1, 200:50
$Comp
L lsf-kicad:LTCC-Balun-06Pins T3
U 1 1 5E5016F9
P 6700 2050
F 0 "T3" V 6375 1575 50  0000 R CNN
F 1 "1720BL15B0200E" V 6300 1900 50  0000 R CNN
F 2 "RF_Converter:Anaren_0805_2012Metric-6" H 6700 2050 50  0001 C CNN
F 3 "~" H 6700 2050 50  0001 C CNN
F 4 "1720BL15B0200E" H 6700 2050 50  0001 C CNN "Mnf."
	1    6700 2050
	0    1    1    0   
$EndComp
$Comp
L lsf-kicad:LTCC-Balun-06Pins T2
U 1 1 5E50CF21
P 5325 4975
F 0 "T2" V 5675 5475 50  0000 L CNN
F 1 "1720BL15B0200E" V 5750 5125 50  0000 L CNN
F 2 "RF_Converter:Anaren_0805_2012Metric-6" H 5325 4975 50  0001 C CNN
F 3 "~" H 5325 4975 50  0001 C CNN
F 4 "1720BL15B0200E" H 5325 4975 50  0001 C CNN "Mnf."
	1    5325 4975
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C5
U 1 1 5E69011D
P 7275 2250
F 0 "C5" H 7390 2296 50  0000 L CNN
F 1 "100p" H 7390 2205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7313 2100 50  0001 C CNN
F 3 "~" H 7275 2250 50  0001 C CNN
F 4 "CBR04C101J3GAC " H 7275 2250 50  0001 C CNN "Mnf."
	1    7275 2250
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 5E6A36E5
P 4725 5150
F 0 "C4" H 4840 5196 50  0000 L CNN
F 1 "100p" H 4840 5105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4763 5000 50  0001 C CNN
F 3 "~" H 4725 5150 50  0001 C CNN
F 4 "CBR04C101J3GAC " H 4725 5150 50  0001 C CNN "Mnf."
	1    4725 5150
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 5E75290C
P 6700 2800
F 0 "R4" V 6907 2800 50  0000 C CNN
F 1 "R" V 6816 2800 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 6630 2800 50  0001 C CNN
F 3 "~" H 6700 2800 50  0001 C CNN
	1    6700 2800
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C2
U 1 1 5E756BB3
P 5325 5600
F 0 "C2" H 5440 5646 50  0000 L CNN
F 1 "100p" H 5440 5555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5363 5450 50  0001 C CNN
F 3 "~" H 5325 5600 50  0001 C CNN
F 4 "CBR04C101J3GAC " H 5325 5600 50  0001 C CNN "Mnf."
	1    5325 5600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C7
U 1 1 5E7572FA
P 6800 4375
F 0 "C7" H 6915 4421 50  0000 L CNN
F 1 "100p" H 6915 4330 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6838 4225 50  0001 C CNN
F 3 "~" H 6800 4375 50  0001 C CNN
F 4 "CBR04C101J3GAC " H 6800 4375 50  0001 C CNN "Mnf."
	1    6800 4375
	1    0    0    -1  
$EndComp
$Comp
L Device:C C6
U 1 1 5E758CD6
P 6700 1450
F 0 "C6" H 6815 1496 50  0000 L CNN
F 1 "100p" H 6815 1405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6738 1300 50  0001 C CNN
F 3 "~" H 6700 1450 50  0001 C CNN
F 4 "CBR04C101J3GAC " H 6700 1450 50  0001 C CNN "Mnf."
	1    6700 1450
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5E7592E7
P 5225 2725
F 0 "C1" H 5110 2679 50  0000 R CNN
F 1 "100p" H 5110 2770 50  0000 R CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5263 2575 50  0001 C CNN
F 3 "~" H 5225 2725 50  0001 C CNN
F 4 "CBR04C101J3GAC " H 5225 2725 50  0001 C CNN "Mnf."
	1    5225 2725
	1    0    0    1   
$EndComp
$Comp
L Device:R R3
U 1 1 5E77742D
P 5325 4425
F 0 "R3" V 5532 4425 50  0000 C CNN
F 1 "R" V 5441 4425 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 5255 4425 50  0001 C CNN
F 3 "~" H 5325 4425 50  0001 C CNN
	1    5325 4425
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5325 5825 5325 5750
Wire Wire Line
	5325 5450 5325 5375
Wire Wire Line
	5325 1700 5325 1225
Wire Wire Line
	6700 1600 6700 1650
$Comp
L Device:L L2
U 1 1 5E80F8B4
P 6700 3075
F 0 "L2" V 6519 3075 50  0000 C CNN
F 1 "L" V 6610 3075 50  0000 C CNN
F 2 "Inductor_SMD:L_0402_1005Metric" H 6700 3075 50  0001 C CNN
F 3 "~" H 6700 3075 50  0001 C CNN
	1    6700 3075
	0    -1   1    0   
$EndComp
$Comp
L Device:L L1
U 1 1 5E810F7B
P 5325 4150
F 0 "L1" V 5515 4150 50  0000 C CNN
F 1 "L" V 5424 4150 50  0000 C CNN
F 2 "Inductor_SMD:L_0402_1005Metric" H 5325 4150 50  0001 C CNN
F 3 "~" H 5325 4150 50  0001 C CNN
	1    5325 4150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6600 2450 6600 2500
Wire Wire Line
	6600 2500 6500 2500
Wire Wire Line
	6800 2500 6800 2450
$Comp
L lsf-kicad:LTCC-Balun-06Pins T4
U 1 1 5E8CD074
P 6700 4975
F 0 "T4" V 7075 4600 50  0000 R CNN
F 1 "2450BL15B050" V 6975 4600 50  0000 R CNN
F 2 "RF_Converter:Anaren_0805_2012Metric-6" H 6700 4975 50  0001 C CNN
F 3 "~" H 6700 4975 50  0001 C CNN
F 4 "2450BL15B050" H 6700 4975 50  0001 C CNN "Mnf."
	1    6700 4975
	0    -1   -1   0   
$EndComp
Text Label 3550 3975 0    50   ~ 0
MIX2_IPP
Text Label 3550 4075 0    50   ~ 0
MIX2_IPN
Text Label 3550 4175 0    50   ~ 0
MIX2_OPN
Text Label 3550 2675 0    50   ~ 0
MIX1_IPN
Text Label 3550 2775 0    50   ~ 0
MIX1_IPP
Text Label 3550 2875 0    50   ~ 0
MIX1_OPN
Text Label 3550 2975 0    50   ~ 0
MIX1_OPP
Text Label 1750 3875 2    50   ~ 0
REXT
Text Label 1800 2675 1    50   ~ 0
LOCK-DO
Wire Wire Line
	4725 5300 4725 5400
Wire Wire Line
	7100 2050 7275 2050
Wire Wire Line
	7275 2050 7275 2100
Wire Wire Line
	7275 2050 7275 1975
Connection ~ 7275 2050
Wire Wire Line
	7275 2400 7275 2475
$Comp
L Device:C C26
U 1 1 5E9CF8C9
P 5825 5200
F 0 "C26" H 5940 5246 50  0000 L CNN
F 1 "100p" H 5940 5155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5863 5050 50  0001 C CNN
F 3 "~" H 5825 5200 50  0001 C CNN
F 4 "CBR04C101J3GAC " H 5825 5200 50  0001 C CNN "Mnf."
	1    5825 5200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C28
U 1 1 5E9CFBE8
P 6600 4375
F 0 "C28" H 6486 4329 50  0000 R CNN
F 1 "100p" H 6486 4420 50  0000 R CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6638 4225 50  0001 C CNN
F 3 "~" H 6600 4375 50  0001 C CNN
F 4 "CBR04C101J3GAC " H 6600 4375 50  0001 C CNN "Mnf."
	1    6600 4375
	1    0    0    1   
$EndComp
$Comp
L Device:C C3
U 1 1 5E9D0CBB
P 5425 2725
F 0 "C3" H 5540 2771 50  0000 L CNN
F 1 "100p" H 5540 2680 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5463 2575 50  0001 C CNN
F 3 "~" H 5425 2725 50  0001 C CNN
F 4 "CBR04C101J3GAC " H 5425 2725 50  0001 C CNN "Mnf."
	1    5425 2725
	1    0    0    -1  
$EndComp
$Comp
L Device:C C27
U 1 1 5E9D1EDC
P 6175 2225
F 0 "C27" H 6061 2179 50  0000 R CNN
F 1 "100p" H 6061 2270 50  0000 R CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6213 2075 50  0001 C CNN
F 3 "~" H 6175 2225 50  0001 C CNN
F 4 "CBR04C101J3GAC " H 6175 2225 50  0001 C CNN "Mnf."
	1    6175 2225
	1    0    0    1   
$EndComp
Wire Wire Line
	5825 5050 5825 4975
Wire Wire Line
	5825 5350 5825 5400
Wire Wire Line
	7175 4975 7100 4975
$Comp
L lsf-kicad:LTCC-Balun-06Pins T1
U 1 1 5E500815
P 5325 2100
F 0 "T1" V 4975 1625 50  0000 R CNN
F 1 "HHM17146A1" V 4900 2000 50  0000 R CNN
F 2 "RF_Converter:Balun_Johanson_1.6x0.8mm" H 5325 2100 50  0001 C CNN
F 3 "~" H 5325 2100 50  0001 C CNN
F 4 "HHM17146A1" H 5325 2100 50  0001 C CNN "Mnf."
	1    5325 2100
	0    1    1    0   
$EndComp
Wire Wire Line
	5825 2275 5825 2100
Wire Wire Line
	5825 2100 5725 2100
Wire Wire Line
	4925 2100 4775 2100
Wire Wire Line
	4775 2100 4775 2225
Wire Wire Line
	5225 2500 5225 2575
Wire Wire Line
	5425 2500 5425 2575
Wire Wire Line
	6800 4525 6800 4575
Wire Wire Line
	6600 4525 6600 4575
Wire Wire Line
	6200 4975 6300 4975
Wire Wire Line
	7175 5400 7175 4975
Wire Wire Line
	6200 4975 6200 5400
Wire Wire Line
	6700 1300 6700 1225
Wire Wire Line
	6800 2500 6900 2500
Wire Wire Line
	6900 2500 6900 2800
Wire Wire Line
	6500 2500 6500 2800
Wire Wire Line
	6550 2800 6500 2800
Connection ~ 6500 2800
Wire Wire Line
	6500 2800 6500 3075
Wire Wire Line
	6550 3075 6500 3075
Connection ~ 6500 3075
Wire Wire Line
	6500 3075 6500 3225
Wire Wire Line
	6850 2800 6900 2800
Connection ~ 6900 2800
Wire Wire Line
	6900 2800 6900 3075
Wire Wire Line
	6850 3075 6900 3075
Connection ~ 6900 3075
Wire Wire Line
	6900 3075 6900 3225
Text Label 3550 4275 0    50   ~ 0
MIX2_OPP
Wire Wire Line
	3450 3975 3550 3975
Wire Wire Line
	3450 4075 3550 4075
Wire Wire Line
	3450 4175 3550 4175
Wire Wire Line
	3450 4275 3550 4275
Wire Wire Line
	3450 2675 3550 2675
Wire Wire Line
	3450 2775 3550 2775
Wire Wire Line
	3450 2875 3550 2875
Wire Wire Line
	3450 2975 3550 2975
Wire Wire Line
	5225 2875 5225 2950
Wire Wire Line
	5425 2875 5425 2950
Wire Wire Line
	5225 4575 5225 4525
Wire Wire Line
	5225 4525 5150 4525
Wire Wire Line
	5425 4575 5425 4525
Wire Wire Line
	5425 4525 5500 4525
Wire Wire Line
	5500 4525 5500 4425
Wire Wire Line
	5175 4150 5150 4150
Wire Wire Line
	5150 3925 5150 4150
Connection ~ 5150 4150
Wire Wire Line
	5150 4150 5150 4425
Wire Wire Line
	5475 4150 5500 4150
Connection ~ 5500 4150
Wire Wire Line
	5500 4150 5500 3925
Wire Wire Line
	5475 4425 5500 4425
Connection ~ 5500 4425
Wire Wire Line
	5500 4425 5500 4150
Wire Wire Line
	5175 4425 5150 4425
Connection ~ 5150 4425
Wire Wire Line
	5150 4425 5150 4525
Wire Wire Line
	6600 4225 6600 4125
Wire Wire Line
	6800 4225 6800 4125
Wire Wire Line
	6700 5375 6700 5825
Wire Wire Line
	4925 4975 4725 4975
Wire Wire Line
	4725 4975 4725 4900
Wire Wire Line
	4725 4975 4725 5000
Connection ~ 4725 4975
Text Label 5225 2950 3    50   ~ 0
MIX1_IPN
Text Label 5425 2950 3    50   ~ 0
MIX1_IPP
Text Label 6500 3225 3    50   ~ 0
MIX1_OPN
Text Label 6900 3225 3    50   ~ 0
MIX1_OPP
Text Label 5150 3925 1    50   ~ 0
MIX2_OPP
Text Label 5500 3925 1    50   ~ 0
MIX2_OPN
Text Label 6600 4125 1    50   ~ 0
MIX2_IPN
Text Label 6800 4125 1    50   ~ 0
MIX2_IPP
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5F0571A4
P 3250 7025
F 0 "#FLG0101" H 3250 7100 50  0001 C CNN
F 1 "PWR_FLAG" H 3250 7198 50  0000 C CNN
F 2 "" H 3250 7025 50  0001 C CNN
F 3 "~" H 3250 7025 50  0001 C CNN
	1    3250 7025
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5F0584DD
P 3575 7175
F 0 "#FLG0102" H 3575 7250 50  0001 C CNN
F 1 "PWR_FLAG" H 3575 7348 50  0000 C CNN
F 2 "" H 3575 7175 50  0001 C CNN
F 3 "~" H 3575 7175 50  0001 C CNN
	1    3575 7175
	-1   0    0    1   
$EndComp
Wire Wire Line
	3575 7175 3575 7025
Wire Wire Line
	3250 7175 3250 7025
Wire Wire Line
	5725 4975 5825 4975
Wire Wire Line
	6175 2075 6175 2050
Wire Wire Line
	6175 2050 6300 2050
Text Label 6700 1300 0    50   ~ 0
IF-RX
Text Label 5325 1700 0    50   ~ 0
RF-RX
Text Label 6700 5825 0    50   ~ 0
IF-TX
Text Label 5325 5825 0    50   ~ 0
RF-TX
Wire Wire Line
	1850 2775 1750 2775
Text Label 1750 2775 3    50   ~ 0
FM
$Comp
L Device:C C29
U 1 1 5E3D3D0F
P 1250 5875
F 0 "C29" H 1365 5921 50  0000 L CNN
F 1 "33p" H 1365 5830 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 1288 5725 50  0001 C CNN
F 3 "~" H 1250 5875 50  0001 C CNN
F 4 "C0402C330J4RACAUTO " H 1250 5875 50  0001 C CNN "Mnf."
	1    1250 5875
	1    0    0    -1  
$EndComp
$Comp
L Device:C C30
U 1 1 5E3D4485
P 3900 5875
F 0 "C30" H 4015 5921 50  0000 L CNN
F 1 "33p" H 4015 5830 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3938 5725 50  0001 C CNN
F 3 "~" H 3900 5875 50  0001 C CNN
F 4 "C0402C330J4RACAUTO " H 3900 5875 50  0001 C CNN "Mnf."
	1    3900 5875
	1    0    0    -1  
$EndComp
Wire Wire Line
	3525 6175 3900 6175
Wire Wire Line
	3900 6175 3900 6025
Connection ~ 3525 6175
Wire Wire Line
	1600 6175 1250 6175
Wire Wire Line
	1250 6175 1250 6025
Connection ~ 1600 6175
Wire Wire Line
	1250 5725 1250 5550
Wire Wire Line
	3900 5725 3900 5550
Text Label 3900 5725 2    50   ~ 0
FM
Text Label 1250 5725 2    50   ~ 0
LOCK-DO
Text Notes 8300 4575 0    50   ~ 0
TCXO Oscillator: 26MHz
Text Notes 3925 3800 0    50   ~ 0
L1, R3: optional, \noptimize the impidance matching\nApplication Note: \nIntegrated Synthesizer/Mixer\nMatching Circuits and Baluns
Text Notes 6950 3125 0    50   ~ 0
L2, R4: optional, \noptimize the impidance matching\nApplication Note: \nIntegrated Synthesizer/Mixer\nMatching Circuits and Baluns
Text Notes 7050 4325 0    50   ~ 0
C28, C7: AC coupling\ncapacitors. In case of\nLTCC baluns, must be\nplaced in balanced lines\nApplication Note: \nIntegrated Synthesizer/Mixer\nMatching Circuits and Baluns
Text Notes 3975 3100 0    50   ~ 0
C1, C3: AC coupling\ncapacitors. In case of\nLTCC baluns, must be\nplaced in balanced lines.\nApplication Note: \nIntegrated Synthesizer/Mixer\nMatching Circuits and Baluns
Text Notes 7425 1700 0    50   ~ 0
C5, C27, C6: AC coupling\ncapacitors.\nApplication Note: \nIntegrated Synthesizer/Mixer\nMatching Circuits and Baluns
Text Notes 3500 5275 0    50   ~ 0
C4, C26, C2: AC coupling\ncapacitors.\nApplication Note: \nIntegrated Synthesizer/Mixer\nMatching Circuits and Baluns
Text Notes 4700 7725 0    50   ~ 0
Note that any narrowband solution\nwill have the disadvantage that the\nmixer ports will not have a balanced\nload across all frequencies. This could\nmean that the LO and RF rejection at\nthe output port are not as good as \nthey are for a wideband transformer.\nThe common mode rejection ratio\n (CMRR) of the output balun and \nmatching circuit is important, as this\ncan affect the noise floor and \nspurious levels at the mixer\noutput. The CMRR of the balun\nwill degrade as phase and\namplitude imbalance increase.
Text Notes 9475 2250 0    50   ~ 0
The filter is designed to give\nlowest integrated phase noise,\nfor reference frequencies of\nbetween 26MHz and 52MHz.
Wire Wire Line
	8775 2650 8775 2800
Wire Wire Line
	8775 3275 9150 3275
Wire Wire Line
	9150 3275 9150 2750
Connection ~ 8775 3275
Connection ~ 9150 2750
Wire Notes Line
	6350 2725 8225 2725
Wire Notes Line
	8225 2725 8225 3175
Wire Notes Line
	8225 3175 6350 3175
Wire Notes Line
	6350 3175 6350 2725
Wire Notes Line
	3900 3375 3900 4525
Wire Notes Line
	3900 4525 5675 4525
Wire Notes Line
	5675 4525 5675 3375
Wire Notes Line
	5675 3375 3900 3375
Text HLabel 6700 5825 3    50   Input ~ 0
IF-TX
Text HLabel 5325 5825 3    50   Input ~ 0
RF-TX
Text HLabel 5325 1225 1    50   Input ~ 0
RF-RX
Text HLabel 6700 1225 1    50   Input ~ 0
IF-RX
Text HLabel 3575 7025 1    50   Input ~ 0
VDD
Text HLabel 4725 4900 1    50   Input ~ 0
VDD
Text HLabel 9525 3825 1    50   Input ~ 0
VDD
Text HLabel 10400 4175 1    50   Input ~ 0
VDD
Text HLabel 7275 1975 1    50   Input ~ 0
VDD
Text HLabel 2650 975  1    50   Input ~ 0
VDD
Text HLabel 2650 2325 1    50   Input ~ 0
VDD
Text HLabel 1250 5550 1    50   Input ~ 0
LOCK-DO
Text HLabel 1600 5550 1    50   Input ~ 0
RST
Text HLabel 1975 5550 1    50   Input ~ 0
ENX
Text HLabel 2375 5550 1    50   Input ~ 0
SCLK
Text HLabel 2775 5550 1    50   Input ~ 0
SDA
Text HLabel 3150 5550 1    50   Input ~ 0
MODE
Text HLabel 3525 5550 1    50   Input ~ 0
ENBL
Text HLabel 3900 5550 1    50   Input ~ 0
FM
Text HLabel 3250 7175 3    50   Input ~ 0
GND
Text HLabel 2575 6250 3    50   Input ~ 0
GND
Text HLabel 4725 5400 3    50   Input ~ 0
GND
Text HLabel 5825 5400 3    50   Input ~ 0
GND
Text HLabel 6200 5400 3    50   Input ~ 0
GND
Text HLabel 7175 5400 3    50   Input ~ 0
GND
Text HLabel 9525 5100 3    50   Input ~ 0
GND
Text HLabel 10400 4675 3    50   Input ~ 0
GND
Text HLabel 10450 3275 3    50   Input ~ 0
GND
Text HLabel 9850 3275 3    50   Input ~ 0
GND
Text HLabel 7275 2475 3    50   Input ~ 0
GND
Wire Wire Line
	6175 2375 6175 2475
Text HLabel 6175 2475 3    50   Input ~ 0
GND
Text HLabel 5825 2275 3    50   Input ~ 0
GND
Text HLabel 4775 2225 3    50   Input ~ 0
GND
Text HLabel 2650 1725 3    50   Input ~ 0
GND
Text HLabel 925  2850 3    50   Input ~ 0
GND
Text HLabel 925  3975 3    50   Input ~ 0
GND
Text HLabel 2650 4750 3    50   Input ~ 0
GND
$Comp
L power:PWR_FLAG #FLG0103
U 1 1 5E7669B3
P 6175 2050
F 0 "#FLG0103" H 6175 2125 50  0001 C CNN
F 1 "PWR_FLAG" H 6175 2223 50  0000 C CNN
F 2 "" H 6175 2050 50  0001 C CNN
F 3 "~" H 6175 2050 50  0001 C CNN
	1    6175 2050
	1    0    0    -1  
$EndComp
Connection ~ 6175 2050
$Comp
L power:PWR_FLAG #FLG0104
U 1 1 5E7679A4
P 5825 4975
F 0 "#FLG0104" H 5825 5050 50  0001 C CNN
F 1 "PWR_FLAG" H 5825 5148 50  0000 C CNN
F 2 "" H 5825 4975 50  0001 C CNN
F 3 "~" H 5825 4975 50  0001 C CNN
	1    5825 4975
	1    0    0    -1  
$EndComp
Connection ~ 5825 4975
$EndSCHEMATC
